
; Drupal version
core = 6.x
api = 2

; Contrib modules
projects[admin][subdir] = contrib
projects[admin][version] = 2.0
projects[adminrole][subdir] = contrib
projects[adminrole][version] = 1.3
projects[advanced_help][subdir] = contrib
projects[advanced_help][version] = 1.2
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.8
projects[debut][subdir] = contrib
projects[debut_admin][subdir] = contrib
projects[features][subdir] = contrib
projects[features][version] = 1.0
projects[poormanscron][subdir] = contrib
projects[poormanscron][version] = 2.2
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0
projects[views][subdir] = contrib
projects[views][version] = 2.11
